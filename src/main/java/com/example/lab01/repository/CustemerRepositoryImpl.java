package com.example.lab01.repository;

import com.example.lab01.core.domain.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CustemerRepositoryImpl implements CustemerRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustemerRepositoryImpl.class);

    @Override
    public String getById(int id) {
        return "Juan Morales";
    }

    @Override
    public List<Customer> getList() {
        List<Customer> customers = new ArrayList<>();

        Customer customer = new Customer();
        customer.setCustoerId(1);
        customer.setNombre("Juan");
        customers.add(customer);

        Customer customer2 = new Customer();
        customer2.setCustoerId(2);
        customer2.setNombre("Miguel");
        customers.add(customer2);

        return customers;
    }


    @Override
    public Customer getCustomerById(int id){
        Customer customer = new Customer();
        customer.setCustoerId(id);
        customer.setNombre("Juan");
        return customer;
    }

    @Override
    public void save(Customer customer) {
        LOGGER.debug("save");
    }

    @Override
    public void update(Customer customer) {
        LOGGER.debug("update {}", customer.getCustoerId());

    }

    @Override
    public void delete(Customer customer) {
        LOGGER.debug("delete {}", customer.getCustoerId());

    }

}
