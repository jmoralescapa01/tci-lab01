package com.example.lab01.repository;


import com.example.lab01.core.domain.Customer;

import java.util.List;

public interface CustemerRepository {

    String getById(int id);

    List<Customer> getList();

    Customer getCustomerById(int id);

    void save(Customer customer);

    void update(Customer customer);

    void delete(Customer customer);

}
