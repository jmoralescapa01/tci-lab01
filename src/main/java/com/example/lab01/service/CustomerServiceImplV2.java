package com.example.lab01.service;

import com.example.lab01.config.Lab01Properties;
import com.example.lab01.core.domain.Customer;
import com.example.lab01.repository.CustemerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImplV2 implements  CustomerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImplV2.class);

    private CustemerRepository custemerRepository;
    private CryptoService cryptoService;
    private Lab01Properties lab01Properties;

    public CustomerServiceImplV2(CustemerRepository custemerRepository,
                                    CryptoService cryptoService, Lab01Properties lab01Properties) {
        this.custemerRepository = custemerRepository;
        this.cryptoService= cryptoService;
        this.lab01Properties = lab01Properties;
    }

    @Override
    public String upper(String input) {
        return input.toLowerCase() + custemerRepository.getById(2);
    }

    @Override
    public String upper2(String input) {
        return input.toUpperCase();
    }

    public List<Customer> getList(){
        return custemerRepository.getList();
    }

    public Customer getById(int id) {
        return custemerRepository.getCustomerById(id);
    }

    @Override
    public void save(Customer customer) {
        if(lab01Properties.isPanicEnabled()) {
            throw new RuntimeException();
        }

        LOGGER.debug("save");
        customer.setPassword(cryptoService.encrypt(customer.getPassword()));
        custemerRepository.save(customer);
    }

    @Override
    public void update(Customer customer) {
        LOGGER.debug("update {}", customer.getCustoerId());
        custemerRepository.update(customer);
    }

    @Override
    public void detele(Customer customer) {
        LOGGER.debug("delete {}", customer.getCustoerId());
        custemerRepository.delete(customer);
    }

}
