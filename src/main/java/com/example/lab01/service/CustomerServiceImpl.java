package com.example.lab01.service;

import com.example.lab01.core.domain.Customer;
import com.example.lab01.repository.CustemerRepository;
import java.util.List;


public class CustomerServiceImpl {

    private CustemerRepository custemerRepository;

    public CustomerServiceImpl(CustemerRepository custemerRepository) {
        this.custemerRepository = custemerRepository;
    }

    public String upper(String input) {
        return input.toUpperCase();
    }

    public List<Customer> getListCustomers(){
        return custemerRepository.getList();
    }

}
