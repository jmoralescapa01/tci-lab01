package com.example.lab01.service;

import com.example.lab01.core.domain.Customer;

import java.util.List;

public interface CustomerService {

    String upper(String input);

    String upper2(String input);

    List<Customer> getList();

    Customer getById(int id);

    void save (Customer customer);

    void update(Customer customer);

    void detele(Customer customer);


}
