package com.example.lab01.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(Lab01Properties.class)
public class LabConfiguration {

}
