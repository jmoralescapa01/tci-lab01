package com.example.lab01.config;

import org.hibernate.validator.constraints.Length;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ConfigurationProperties("lab01.features")
@Validated
public class Lab01Properties {

    public CryptoProperties cryptoProperties;

    @NotNull(message = "Objeto SMTP1 no debe ser nulo")
    public MailProperties smtp1;

    @NotNull(message = "Objeto SMTP2 no debe ser nulo")
    public MailProperties smtp2;

    @NotNull(message = "Objeto APIS no debe ser nulo")
    public List<ApiBean> apis;

    private boolean panicEnabled;

    public CryptoProperties getCrypto() {
        return cryptoProperties;
    }

    public void setCrypto(CryptoProperties cryptoProperties) {
        this.cryptoProperties = cryptoProperties;
    }

    public boolean isPanicEnabled() {
        return panicEnabled;
    }

    public void setPanicEnabled(boolean panicEnabled) {
        this.panicEnabled = panicEnabled;
    }

    public MailProperties getSmtp1() {
        return smtp1;
    }

    public void setSmtp1(MailProperties smtp1) {
        this.smtp1 = smtp1;
    }

    public MailProperties getSmtp2() {
        return smtp2;
    }

    public void setSmtp2(MailProperties smtp2) {
        this.smtp2 = smtp2;
    }

    public List<ApiBean> getApis() {
        return apis;
    }

    public void setApis(List<ApiBean> apis) {
        this.apis = apis;
    }

    public static class CryptoProperties{

        @NotEmpty(message = "Crypto service key no puede ser nulo")
        @Length(min = 10, max = 30)
        private String key;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

    }

    public static class MailProperties {

        @NotEmpty(message = "Host no puede ser nulo")
        private String host;

        @NotEmpty(message = "CC no puede ser nulo")
        private List<String> cc;

        @Min(8080)
        @Max(9090)
        private int port;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public List<String> getCc() {
            return cc;
        }

        public void setCc(List<String> cc) {
            this.cc = cc;
        }

        public int getPort() {
            return port;
        }

        public void setPort(int port) {
            this.port = port;
        }
    }

    public static class ApisProperties {

        @NotEmpty(message = "APIS no puede ser nulo")
        private List<ApiBean> apis;

        public List<ApiBean> getApis() {
            return apis;
        }

    }

    public static class ApiBean {
        private String url;
        private Credential credential;
        private HashMap<String, String> headers;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Credential getCredential() {
            return credential;
        }

        public void setCredential(Credential credential) {
            this.credential = credential;
        }

        public HashMap<String, String> getHeaders() {
            return headers;
        }

        public void setHeaders(HashMap<String, String> headers) {
            this.headers = headers;
        }
    }

    public static class Credential {
        private String user;
        private String password;

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public static class Header {
        private String header;

        public String getHeader() {
            return header;
        }

        public void setHeader(String header) {
            this.header = header;
        }
    }

}
