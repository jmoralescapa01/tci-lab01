package com.example.lab01.controller.web.dto;

import com.example.lab01.core.domain.Customer;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomersWebDto {

    private List<Customer> customers;

    public CustomersWebDto(List<Customer> listCustomers) {
        this.customers=listCustomers;

    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
