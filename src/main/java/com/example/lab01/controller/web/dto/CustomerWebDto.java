package com.example.lab01.controller.web.dto;

import com.example.lab01.core.domain.Customer;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomerWebDto {

//    P1s05TicH$$.2011
    private Customer customer;

    public CustomerWebDto() {

    }

    public CustomerWebDto(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}
