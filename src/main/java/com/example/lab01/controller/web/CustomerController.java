package com.example.lab01.controller.web;

import com.example.lab01.controller.web.dto.CustomerWebDto;
import com.example.lab01.controller.web.dto.CustomersWebDto;
import com.example.lab01.core.domain.Customer;
import com.example.lab01.service.CustomerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerService customerService;
    public static final String X_API_FORCE_SYC_HEADER = "X-Api-Force-Sync";

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public HttpEntity<CustomersWebDto> getList() {
        List<Customer> customerList = customerService.getList();
        if (customerList!=null){
            CustomersWebDto customersWebDto = new CustomersWebDto(customerList);
            return new ResponseEntity<>(customersWebDto, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/{id}")
    public HttpEntity<CustomerWebDto> getById(@PathVariable("id") int id){
        Customer customer = customerService.getById(id);
        if(customer!=null){
            CustomerWebDto customerWebDto = new CustomerWebDto(customer);
            return new ResponseEntity<>(customerWebDto,HttpStatus.OK);

        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


//    @PostMapping
//    public ResponseEntity<Void> create(@RequestBody Customer customer) {
//        if (customer!=null) {
//            StringUtils.capitalize(customer.getNombre());
//            customerService.save(customer);
//            return new ResponseEntity<>(HttpStatus.CREATED);
//        } else {
//            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
//        }
//    }

    @PostMapping
    public HttpEntity<CustomerWebDto> create(
            @RequestHeader(name = X_API_FORCE_SYC_HEADER, required = false, defaultValue = "false") boolean forceSync,
            @RequestBody CustomerWebDto customerWebDto) {

        customerService.save(customerWebDto.getCustomer());

        if (forceSync){
            return ResponseEntity.ok().build();
        }else{
            return ResponseEntity.accepted().build();
        }

    }

    @PutMapping("{id}")
    public ResponseEntity<Void> update(@PathVariable(name="id") int id, @RequestBody Customer customer) {
        if (customer!=null) {
            customerService.update(customer);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable(name="id") Integer id) {
        if ( id!=null && id>0 ) {
            Customer customer = new Customer();
            customer.setCustoerId(id);
            customerService.detele(customer);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

    }

}