package com.example.lab01.service;

import com.example.lab01.config.Lab01Properties;
import com.example.lab01.core.domain.Customer;
import com.example.lab01.repository.CustemerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {CustomerServiceImplV2Test.Configuration.class})
public class CustomerServiceImplV2Test {

    static class Configuration {
        @Bean
        public Lab01Properties lab01Properties() {
            Lab01Properties lab01Properties = new Lab01Properties();
            return lab01Properties;
        }

        @Bean
        public CustomerService customerService(CustemerRepository custemerRepository, CryptoService cryptoService, Lab01Properties lab01Properties) {
            return new CustomerServiceImplV2(custemerRepository, cryptoService, lab01Properties);
        }
    }

    @MockBean
    private CustemerRepository custemerRepository;

    @MockBean
    private CryptoService cryptoService;

    @Autowired
    private CustomerService customerService;

    @BeforeEach
    void cleanCachesAndMocksAndStubs() {
        //Habilita modo pánico
        lab01Properties.setPanicEnabled(false);
        reset(custemerRepository, cryptoService);
    }

    @Autowired
    private Lab01Properties lab01Properties;

    @Test
    public void upper_test(){
        String actual = customerService.upper2("a");
        assertThat(actual).isEqualTo("A");
    }



    @Test
    public void getList_ok(){
        // Preparing data
        Customer customer1 = new Customer();
        customer1.setCustoerId(1);
        customer1.setNombre("nombre1");
        customer1.setPaterno("paterno1");

        Customer customer2 = new Customer();
        customer2.setCustoerId(2);
        customer2.setNombre("nombre2");
        customer2.setPaterno("paterno2");

        List<Customer> list = Arrays.asList(customer1,customer2);

        // Mocks & Stubs configuration
        when(custemerRepository.getList()).thenReturn(list);

        // Business logic execution
        List<Customer> customerList = customerService.getList();

        // Validating results
        verify(custemerRepository).getList();
        verifyNoMoreInteractions(custemerRepository);

        // Validating mocks behaviour
        assertThat(customerList).isEqualTo(list);

    }

    @Test
    public void getById_ok(){
        // Preparing data
        Customer customer1 = new Customer();
        customer1.setCustoerId(1);
        customer1.setNombre("nombre1");
        customer1.setPaterno("paterno1");

        // Mocks & Stubs configuration
        when(custemerRepository.getCustomerById(1)).thenReturn(customer1);

        // Business logic execution
        Customer customer = customerService.getById(1);

        // Validating results
        verify(custemerRepository).getCustomerById(1);
        verifyNoMoreInteractions(custemerRepository);

        // Validating mocks behaviour
        assertThat(customer).isEqualTo(customer1);
    }


    @Test
    public void getSave_ok() {
        // Preparing data
        Customer customer = new Customer();
        customer.setCustoerId(1);
        customer.setNombre("nombre1");
        customer.setPaterno("paterno1");
        customer.setPassword("passwordClear");

        Customer expected = new Customer();
        expected.setCustoerId(1);
        expected.setNombre("nombre1");
        expected.setPaterno("paterno1");
        expected.setPassword("passwordCipher");

        // Mocks & Stubs configuration
        doNothing().when(custemerRepository).save(expected);
//        when(cryptoService.encrypt("passwordClear")).thenReturn("passwordCipher");
        when(cryptoService.encrypt("passwordClear")).thenReturn("passwordCipher");

        // Business logic execution
        customerService.save(customer);

        // Validating results
        verify(custemerRepository).save(expected);
        verify(cryptoService).encrypt("passwordClear");
        verifyNoMoreInteractions(custemerRepository,cryptoService);

        // Validating mocks behaviour
//        assertThat(customer.getPassword()).isEqualTo(cryptoService.decrypt(expected.getPassword()));

    }

    @Test
    public void save_inPanic_ok () {
        // Preparing data
        lab01Properties.setPanicEnabled(false);

        Customer customer = new Customer();
        customer.setCustoerId(1);
        customer.setNombre("nombre1");
        customer.setPaterno("paterno1");
        customer.setPassword("passwordClear");

        // Mocks & Stubs configuration

        // Business logic execution
        assertThrows(RuntimeException.class,()->{
            customerService.save(customer);
        });

        // Validating results

        // Validating mocks behaviour

    }

    //Estructura de  test
    // Preparing data
    // Mocks & Stubs configuration
    // Business logic execution
    // Validating results
    // Validating mocks behaviour

}
